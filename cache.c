//------------------------------------------------------------------------------
// 4190.308                     Computer Architecture                  Fall 2015
//
// Cache Simulator Lab
//
// File: cache.c
//
// (C) 2015 Computer Systems and Platforms Laboratory, Seoul National University
//
// Changelog
// 20151119   bernhard    created
//

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "cache.h"

char RP_STR[RP_MAX+1][32] = {
  "round robin", "random", "LRU (least-recently used)",
  "MRU (most-recently used)", "LFU (least-frequently used)",
  "MFU (most-frequently used)"
};

char WP_STR[WP_MAX+1][20] = {
  "write-allocate", "no write-allocate"
};


Cache* create_cache(uint32 capacity, uint32 blocksize, uint32 ways,
                    uint32 rp, uint32 wp, uint32 verbosity)
{
  // 1. check cache parameters
  //    - capacity, blocksize, and ways must be powers of 2
  assert(ISPOW2(capacity));
  assert(ISPOW2(blocksize));
  assert(ISPOW2(ways));
  //    - capacity must be >= blocksize
  assert(blocksize <= capacity);
  //    - number of ways must be <= the number of blocks
  assert(capacity / blocksize >= ways);

  // 2. allocate cache and initialize them
  //    - use the above data structures Cache, Set, and Line
  uint32 i,j;
  uint32 sets = capacity / blocksize / ways;
  uint32 tagshift = 0;
  Cache *c = (Cache *)calloc(1, sizeof(Cache));
  for (i = 1; i < capacity / ways; i <<= 1) {
    tagshift++;
  }
  c->m_capacity = capacity;
  c->m_blocksize = blocksize;
  c->m_ways = ways;
  c->m_rp = rp;
  c->m_wp = wp;
  c->m_verbose = verbosity;
  c->set = (Set *)calloc(sets, sizeof(Set));
  for (i = 0; i < sets; ++i) {
    switch (rp) {
    case RP_RR:
      (&c->set[i])->meta = (uint32 *)calloc(1, sizeof(uint32));
      break;
    case RP_LRU:
      (&c->set[i])->meta = (uint32 *)calloc(ways, sizeof(uint32));
      for (j = 0; j < ways; ++j) (&c->set[i])->meta[j] = j;
      break;
    }
    (&c->set[i])->way = (Line *)calloc(ways, sizeof(Line));
    for (j = 0; j < ways; ++j) {
      (&(&c->set[i])->way[j])->set_idx = i;
      (&(&c->set[i])->way[j])->line_idx = j;
    }
  }

  // 3. print cache configuration
  printf("Cache configuration:\n"
         "  capacity:        %6u\n"
         "  blocksize:       %6u\n"
         "  ways:            %6u\n"
         "  sets:            %6u\n"
         "  tag shift:       %6u\n"
         "  replacement:     %s\n"
         "  on write miss:   %s\n"
         "\n",
         capacity, blocksize, ways, sets, tagshift, RP_STR[rp], WP_STR[wp]);

  // 4. return cache
  return c;
}

void delete_cache(Cache *c)
{
  // free every allocated memories in cache
  uint32 i;
  uint32 sets = c->m_capacity / c->m_blocksize / c->m_ways;
  for (i = 0; i < sets; ++i) {
    free((&c->set[i])->way);
    switch (c->m_rp) {
    case RP_RR:
    case RP_LRU:
      free((&c->set[i])->meta);
      break;
    }
  }
  free(c->set);
  free(c);
}

void line_access(Cache *c, Line *l)
{
  // update data structures to reflect access to a cache line
  uint32 set = l->set_idx;
  uint32 line = l->line_idx;
  uint32 *meta = (&c->set[set])->meta;
  uint32 i;
  switch (c->m_rp) {
  case RP_LRU:
    // true LRU: meta[0] stores the least-recently used index
    for (i=0; i < c->m_ways-1; ++i) {
      if (meta[i] == line) {
        for (; i < c->m_ways-1; ++i) {
          meta[i] = meta[i+1];
        }
        break;
      }
    }
    meta[i] = line;
    // pseudo LRU
    /*
    line += c->m_ways;
    while (line > 1) {
      i = (line+1)%2;
      line /= 2;
      meta[line] = i;
    }
    */
    break;
  }
}


void line_alloc(Cache *c, Line *l, uint32 tag)
{
  // update data structures to reflect allocation of a new block into a line
  if (l->valid == 0) {
    l->valid = 1;
  }
  l->tag = tag;
}

uint32 set_find_victim(Cache *c, Set *s)
{
  // for a given set, return the victim line where to place the new block
  uint32 i;
  c->s_evict++;
  switch (c->m_rp) {
  case RP_RR:
    // round-robin
    i = s->meta[0];
    s->meta[0]++;
    s->meta[0] = s->meta[0] % c->m_ways;
    return i;
  case RP_RANDOM:
    // random
    return rand() % c->m_ways;
  case RP_LRU:
    // true LRU
    return s->meta[0];
    // pseudo LRU
    /*
    i = 1;
    while (i < c->m_ways) {
      i = 2*i+s->meta[i];
    }
    return i-c->m_ways;
    */
  default:
    return 0;
  }
}

void cache_access(Cache *c, uint32 type, uint32 address, uint32 length)
{
  // simulate a cache access
  // 1. compute set & tag
  uint32 i;
  uint32 tagshift = 0;
  uint32 blkshift = 0;
  for (i = 1; i < c->m_capacity / c->m_ways; i <<= 1) ++tagshift;
  for (i = 1; i < c->m_blocksize; i <<= 1) ++blkshift;
  uint32 tagbit = address >> tagshift;
  uint32 setbit = (address & ~(~0 << tagshift)) >> blkshift;
  // uint32 offset = address & ~(~0 << blkshift);

  // 2. check if we have a cache hit
  uint8 hit;
  Line *line;
  Set *set;
  hit = 0;
  set = &c->set[setbit];
  for (i = 0; i < c->m_ways; ++i) {
    if ((&set->way[i])->valid == 0) break;
    if ((&set->way[i])->tag == tagbit) {
      ++hit;
      break;
    }
  }

  // 3. on a cache miss, find a victim block and allocate according to the
  //    current policies
  if (hit || type == READ || c->m_wp == WP_WRITEALLOC) {
    if (i == c->m_ways) i = set_find_victim(c, set);
    line = &set->way[i];
    if (!hit) {
      line_alloc(c, line, tagbit);
    }
    line_access(c, line);
  }

  // 4. update statistics (# accesses, # hits, # misses)
  c->s_access++;
  if (hit) c->s_hit++;
  else c->s_miss++;
}
