#!/bin/bash

MYBIN="cachesim"
REFBIN="cachesim-ref"
EXCLD="*cache.c:??"

for f in $(ls traces); do
    if [[ "$f" == *.bz2 ]]; then
        INPUT="bunzip2 -c traces/$f"
    else
        INPUT="cat traces/$f"
    fi
    for W in `seq 0 1`; do
        for r in `seq 0 2`; do
            for c in `seq 1 16`; do
                for b in `seq 1 16`; do
                    for w in `seq 1 16`; do
                        ARGS="-c $((2**c)) -b $((2**b)) -w $((2**w)) -r $r -W $W"
                        MY="$($INPUT | ./$MYBIN $ARGS 2>&1)"
                        REF="$($INPUT | ./$REFBIN $ARGS 2>&1)"
                        if [[ ${MY#$MYBIN$EXCLD} != ${REF#$REFBIN$EXCLD} ]]; then
                            echo -e "something wrong!\n\nmy result:"
                            echo "${MY#$MYBIN$EXCLD}"
                            echo -e "\nreferaence result:"
                            echo "${REF#$REFBIN$EXCLD}"
                            echo -e "\n\n"
                        fi
                    done
                done
            done
        done
    done
done
echo -e "Done."
